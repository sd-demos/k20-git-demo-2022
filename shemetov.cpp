int factorial(int n) {
    /*
     * Calculates n!
     * in classic recursive way
     */
    if (n == 1) {
        return 1;
    }
    return n * factorial(n - 1);
}