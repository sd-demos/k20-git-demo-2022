cmake_minimum_required(VERSION 3.23)
project(GitDemo)

set(CMAKE_CXX_STANDARD 14)

add_executable(GitDemo main.cpp)
